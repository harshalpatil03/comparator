/* /******************************************************************************
* File Name: solution.c
*
* Description: This file contains code to compare two binary files.
*
*******************************************************************************/

#include <stdio.h>
#include <time.h>

 
int main(int argc, char *argv[])
{
    FILE *fp1, *fp2;
    char file1_char, file2_char, char_arr[40];
    int diff_file=0;
    long position=0;
    clock_t t;
    t = clock();
  
    if (argc < 3)
    {
        printf("\n Please enter both file  \n");
        return 0;
    }
    else
    {
        fp1 = fopen(argv[1],  "r");
        if (fp1 == NULL)
        {
            printf("\nFile not present or file loading error%s", argv[1]);
            return 0;
        }
 
        fp2 = fopen(argv[2], "r");
 
        if (fp2 == NULL)
        {
            printf("\nFile not present or file loading error%s", argv[2]);
            return 0;
        }
 
        if ((fp1 != NULL) && (fp2 != NULL))
        {
        /* the scenario when the difference between starting point and total length is less than 16 bytes, the available bytes are shown*/
        	 
    		while (((file1_char = fgetc(fp1)) != EOF) &&((file2_char = fgetc(fp2)) != EOF))
    		{
        		if (file1_char != file2_char)
        		{ 
        			char c;
        			
        			fseek(fp1, -1, SEEK_CUR);
        			position=ftell(fp1);	
        			/*if the second half of the byte is incorrect, we need to consider complete byte*/
        			if(position%2==0){
        			fseek(fp1, -1, SEEK_CUR);
        			}
        			for(long i=0;i<32;i++)
        			{
        				c=fgetc(fp1);
        				if(c=='\n'||c==':')
        				{
        					i--;
        					
        				}
        				else if(c!=EOF)
        				{
        					char_arr[i]=c;
        				}
        				char_arr[i+1]='\0';
        			}
        			 
        			printf("\nfirst 16 bytes (in hex) or available bytes before EOF are shown from where the files differ = %s",char_arr);      
            			diff_file = 1;
           			break;	
            		}
    		}
    		if (diff_file == 1)
    		{
        		printf("\nTwo files are not equal and the offset at which the first difference was found is - %ld", position);
    		}
    		else
    		{
        		printf("The files are similar\n ");
    		}
       }
    }
    fclose(fp1);
    fclose(fp2);
    	t = clock() - t;
    	double time_taken =(((double)t)/CLOCKS_PER_SEC)*1000;   
    	printf("\nexecution time in millisec %f \n", time_taken);
}